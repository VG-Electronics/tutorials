<?php
    $items = [
        'dashboard' => 'Dashboard',
        'jobs' => 'Jobs'
    ];
?>
@foreach( $items as $key => $name )
    <a href="{{ route('employer.'.$key.'.index') }}">{{ $name }}</a><br>
@endforeach