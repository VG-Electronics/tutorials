<nav class="navbar navbar-light bg-dark">
    <div class="container">
        <a class="navbar-brand text-light" href="{{ route('home.index') }}">Job Finder</a>
        <div>
            <a href="{{ route('home.index') }}" class="mr-3 {{ Route::current()->getName() == 'home.index' ? 'text-info' : 'text-light' }}">Home</a>
            <a href="{{ route('jobs.index') }}" class="mr-3 {{ Route::current()->getName() == 'jobs.index' ? 'text-info' : 'text-light' }}">Job offers</a>
            @if( Auth::guard('employer')->check() )
                <a href="{{ route('employer.dashboard.index') }}" class="text-light">My Account</a>
            @else
                <a href="{{ route('account.index') }}" class="{{ Route::current()->getName() == 'account.index' ? 'text-info' : 'text-light' }}">Account</a>
            @endif
        </div>
    </div>
</nav>