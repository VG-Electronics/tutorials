<footer>
    <div class="row bg-dark">
        <div class="col text-center text-light py-2">
            Tutorial Website by <a href="https://web-invention.com" target="_blank">Web Invention</a>
        </div>
    </div>
</footer>