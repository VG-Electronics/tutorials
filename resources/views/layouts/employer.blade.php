@extends('layouts.main')

@section('content')
    <div class="container my-3">
        <div class="row">
            <div class="col-md-2 my-2 text-center border-right">
                @include('layouts.partial.menuEmployer')
            </div>
            <div class="col-md-10">
                @yield('panel')
            </div>
        </div>
    </div>
@endsection