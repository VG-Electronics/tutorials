<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Job Finder</title>

  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">

  <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>

<body class="bg-muted">
    @include('layouts.partial.topNavigation')

    @yield('content')

    @include('layouts.partial.footer')

    @yield('footer_css')
    @yield('footer_js')
</body>
</html>