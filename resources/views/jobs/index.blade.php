@extends('layouts.main')

@section('content')

<div class="container">
    <div class="row">
        <div class="col pt-4 pb-2">
            <h1 class="text-primary">Jobs</h1>
            <p>Here are listed active jobs - find your favourite!</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col">
            @forelse( $jobs as $job )
                @include('jobs.partial.jobBrief')
            @empty
                <p>Any active jobs at this time.</p>
            @endforelse
        </div>
    </div>
</div>

@endsection