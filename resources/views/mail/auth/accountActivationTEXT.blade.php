Activate your account
To confirm that email "{{ $user->email }}"" belongs to you, go to the activation link below.
Link - {{ route('account.activate', $user->token) }}