<h1>Activate your account</h1>
<p>To confirm that email <i>{{ $user->email }}</i> belongs to you, click the activation link below.</p>
<a href="{{ route('account.activate', $user->token) }}" class="btn btn-success">Activate account</a>