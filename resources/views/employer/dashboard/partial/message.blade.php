<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <b>{{ $message->title }}</b>
    <p>{{ $message->content }}</p>
    <button type="button" class="close readMessage" data-message-id="{{ $message->id }}" data-dismiss="alert">
        <span>&times;</span>
    </button>
</div>