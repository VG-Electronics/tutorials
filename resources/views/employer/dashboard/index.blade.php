@extends('layouts.employer')

@section('panel')
<div class="row">
    <div class="col">
        <h1>Dashboard</h1>
    </div>
</div>
<div class="row">
    <div class="col">
        @forelse( $messages as $message )
            @include('employer.dashboard.partial.message')
        @empty
            <p>You haven't got any unread messages.</p>
        @endforelse
    </div>
</div>
@endsection

@section('footer_js')
@parent
<script>
    $('.readMessage').click(function(){
        let messageID = $(this).data('message-id')

        $.ajax({
            url: "{{ route('employer.dashboard.readMessage') }}",
            type: "POST",
            data: {
                _token: "{{ csrf_token() }}",
                messageID: messageID
            }
        })
        .done(function(response){
            if( !response.success )
                alert("An error has occured. The message hasn't been marked as read.");
        })
    })
</script>
@endsection