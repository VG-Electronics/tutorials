@extends('layouts.employer')

@section('panel')
<div class="row">
    <div class="col">
        <h1>Jobs</h1>
        <a href="{{ route('employer.jobs.create') }}" class="btn btn-sm btn-success">Create a job</a>
    </div>
</div>
<div class="row mt-3">
    <div class="col">
        @foreach( $jobs as $job )
            <div class="py-2 border-top" style="border-color: grey !important; border-width: 1px !important;">
                <div class="mb-1 form-group form-inline">
                    <a href="{{ route('employer.jobs.show', compact('job')) }}" class="btn btn-sm btn-info">Show</a>&nbsp;
                    <a href="{{ route('employer.jobs.edit', compact('job')) }}" class="btn btn-sm btn-primary">Edit</a>&nbsp;
                    <form action="{{ route('employer.jobs.delete', compact('job')) }}" method="POST">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </div>
                <b>{{ $job->name }}</b><br>
                @if( $job->isActive() )
                    <span class="text-success">Active to {{ $job->activeTo() }}</span>
                @else
                    <span class="text-danger">Not active</span>
                @endif 
            </div>
        @endforeach
    </div> 
</div>
@endsection