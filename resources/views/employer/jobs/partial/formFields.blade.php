<div class="form-group mt-2">
    Name: <input name="name" value="{{ $job->name ?? '' }}" class="w-50 form-control" min-length="5" max-length="200" required></input>
    Description:<br>
    <textarea name="description" class="w-75 form-control" rows="5" min-length="20" max-length="1000" required>{{ $job->description ?? '' }}</textarea>
    <br>
    Active: <input name="active" type="checkbox" {{ isset($job) && $job->active ? 'checked' : '' }}></input><br>
    Active to: <input name="active_to" type="datetime-local" value="{{ isset($job) ? $job->activeToForInput() : date('Y-m-d\TH:i') }}"></input>
</div>