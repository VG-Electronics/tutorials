@extends('layouts.employer')

@section('panel')
<div class="row">
    <div class="col">
        <h1>Job - Preview</h1>
    </div>
</div>
<div class="row mt-2">
    <div class="col">
        <div class="form-group form-inline">
            <a href="{{ route('employer.jobs.edit', compact('job')) }}" class="btn btn-sm btn-primary">Edit</a>&nbsp;
            <form action="{{ route('employer.jobs.delete', compact('job')) }}" method="POST">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
            </form>
        </div>
        
        <div class="my-2">
            <h2>{{ $job->name }}</h2>
            <p>{{ $job->description }}</p>

            @if( $job->isActive() )
                <span class="text-success">Active to {{ $job->activeTo() }}</span>
            @else
                <span class="text-danger">Not active</span>
            @endif 
        </div>
    </div> 
</div>
@endsection