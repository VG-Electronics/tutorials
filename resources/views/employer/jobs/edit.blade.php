@extends('layouts.employer')

@section('panel')
<div class="row">
    <div class="col">
        <h1>Jobs - Edit</h1>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="{{ route('employer.jobs.update', compact('job')) }}" method="POST">
            @csrf
            @include('employer.jobs.partial.formFields')

            <button type="submit" class="btn btn-sm btn-success">Save</button>
        </form>
    </div> 
</div>
@endsection