@extends('layouts.employer')

@section('panel')
<div class="row">
    <div class="col">
        <h1>Jobs - New job</h1>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="{{ route('employer.jobs.store') }}" method="POST">
            @csrf
            @include('employer.jobs.partial.formFields')
            
            <button type="submit" class="btn btn-sm btn-success">Create</button>
        </form>
    </div> 
</div>
@endsection