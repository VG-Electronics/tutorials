@extends('layouts.main')

@section('content')

<div class="container">
    <div class="row">
        <div class="col py-4">
            <h1 class="text-primary">Jobs & Employees finder</h1>
            <p>Just share your job offer and find another excellent employee.</p>
            <hr>
        </div>
    </div>
</div>

@endsection