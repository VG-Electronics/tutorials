@extends('layouts.main')

@section('content')

<div class="container">
    <div class="row my-4">
        <div class="col border-right">
            <h1>Login:</h1>
            <form action="{{ route('account.login') }}" method="POST">
                @csrf
                <label for="email">E-mail:</label>
                <input name="email" type="email" class="form-control w-75" required></input>
                <label for="password">Password:</label>
                <input name="password" type="password" class="form-control w-75" required></input>
                
                @error('login')
                    <p class="text-danger">{{ $message }}</p>
                @enderror
                <button type="submit" class="mt-2 btn btn-primary">Log in</button>
            </form>
        </div> 
        <div class="col">
            <h1>Create account:</h1>
            <form action="{{ route('account.register') }}" method="POST">
                @csrf
                <label for="email">E-mail:</label>
                <input name="email" type="email" class="form-control w-75" required></input>
                <label for="password">Password:</label>
                <input name="password" type="password" class="form-control w-75" required></input>
                <label for="password">Password confirm:</label>
                <input name="password_confirm" type="password" class="form-control w-75" required></input>

                <button type="submit" class="mt-2 btn btn-success">Create account</button>
            </form>
        </div>
    </div>
</div>

@endsection