<?php

require_once  __DIR__ .'/../vendor/autoload.php';

use App\Console\JobsScheduler;

$app = require __DIR__ .'/../bootstrap/app.php';
$app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();	

JobsScheduler::run();
echo 'CRON finished successfully.';