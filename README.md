Repository of [my Tutorial Website](https://tutorial.web-invention.com) which is used as a sandbox for creating tutorials.

**Code is not clean and refactorized.**<br>
**Architecture is very experimental and not orderly.**<br>
**Repository may contain permissions issues.**<br>

Experimental architecture solutions:<br>
- Redirects and Views in Http directory
- Repositories
