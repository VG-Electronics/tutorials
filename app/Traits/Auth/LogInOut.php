<?php
namespace App\Traits\Auth;

use Illuminate\Support\Facades\Auth;

trait LogInOut{

    public function logUserIn($user){
        if( !Auth::guard('employer')->check() )
            Auth::guard('employer')->login($user);
    }

    public function logUserOut(){
        if( Auth::guard('employer')->check() )
            Auth::guard('employer')->logout();
    }
}