<?php
namespace App\Repositories\Providers;

use Illuminate\Support\ServiceProvider;

class MessagesRepositoryServiceProvider extends ServiceProvider{

    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contracts\MessagesRepositoryInterface',
            'App\Repositories\Eloquent\MessagesRepository'
        );
    }
}
