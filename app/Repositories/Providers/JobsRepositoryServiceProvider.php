<?php
namespace App\Repositories\Providers;

use Illuminate\Support\ServiceProvider;

class JobsRepositoryServiceProvider extends ServiceProvider{

    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contracts\JobsRepositoryInterface',
            'App\Repositories\Eloquent\JobsRepository'
        );
    }
}
