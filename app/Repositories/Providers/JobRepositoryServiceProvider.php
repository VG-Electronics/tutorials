<?php
namespace App\Repositories\Providers;

use Illuminate\Support\ServiceProvider;

class JobRepositoryServiceProvider extends ServiceProvider{

    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contracts\JobRepositoryInterface',
            'App\Repositories\Eloquent\JobRepository'
        );
    }
}
