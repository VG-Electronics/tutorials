<?php
namespace App\Repositories\Providers;

use Illuminate\Support\ServiceProvider;

class MessageRepositoryServiceProvider extends ServiceProvider{

    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contracts\MessageRepositoryInterface',
            'App\Repositories\Eloquent\MessageRepository'
        );
    }
}
