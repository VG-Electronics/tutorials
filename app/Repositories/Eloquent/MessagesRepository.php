<?php
namespace App\Repositories\Eloquent;

use App\Models\Message;

use App\Repositories\Contracts\MessagesRepositoryInterface;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessagesRepository implements MessagesRepositoryInterface{

    public function getOne($id){
        return Message::find($id);
    }

    public function unreadMessagesForCurrentUser($limit = 10){
        return Auth::user()->messages()->Unread()->latest()->get();
    }
}