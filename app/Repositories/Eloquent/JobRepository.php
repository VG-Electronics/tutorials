<?php
namespace App\Repositories\Eloquent;

class JobRepository implements \JobRepositoryInterface{

    public function saveBasedOnRequest(\Request $request, $job = null){
        
        $job = $job ?: \Auth::guard('employer')->user()->jobs()->make();

        $job->fill( $request->all() );
        $job->active = isset($request->active);
        $job->save();

        return $job;
    }

    public function delete($job){
        $job->delete();
    }
}