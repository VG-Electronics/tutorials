<?php
namespace App\Repositories\Eloquent;

use App\Models\Job;

class JobsRepository implements \JobsRepositoryInterface{

    public function allActive($limit = null){
        return Job::Active()->get();
    }

    public function createdByCurrentUser($limit = null){
        
        return \Auth::user()->jobs;
    }
}