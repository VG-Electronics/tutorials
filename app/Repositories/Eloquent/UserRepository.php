<?php
namespace App\Repositories\Eloquent;

use App\Models\User;

use App\Generators\TokenGenerator;

use App\Repositories\Contracts\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface{

    public $user;

    private $onlyActive = false;

    public function __construct(/*UserQueriesManager $queriesManager, UserQueryFilters $queryFilters*/){
        /*$this->queries = $queryFilters;
        $this->filters = $queriesManager;*/
    }

    public function setUser(User $user){
        $this->user = $user;

        return $this;
    }

    public function onlyActive($onlyActive){
        $this->onlyActive = $onlyActive;

        return $this;
    }

    public function findByProperty($property, $value){

        $user = User::where($property, $value)
            ->when($this->onlyActive, function($query){
                return $query->Active();
            })->first();

        $this->onlyActive(false);

        return $user;
    }

    /*public function findByEmail(string $email, $onlyActive = false){
        return User::when($onlyActive, function($query){
            return $query->Active();
        })->Email($email)->first();
    }*/

    public function saveBasedOnRequest(\Request $request, $user = null){

        $this->user = $user ?: User::make();
        $this->generateToken();
        $this->setPasswordHash($request->password);
        $this->user->fill( $request->all() );
        $this->user->save();

        return $this->user;
    }

    public function generateToken(){
        do{
            $this->user->token = $token = TokenGenerator::createToken();
        }while( User::where('token', $token)->exists() );
    }

    public function setPasswordHash($password){
        $this->user->password = password_hash($password, PASSWORD_DEFAULT);
    }

    public function activate($user){

        $user->active = true;
        $user->token = null;
        $user->save();

        return $user;
    }
}