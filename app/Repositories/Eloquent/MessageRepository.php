<?php
namespace App\Repositories\Eloquent;

use App\Models\Message;

use App\Repositories\Contracts\MessageRepositoryInterface;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageRepository implements MessageRepositoryInterface{

    public function saveAsRead($message){
        $message->read = true;
        $message->save();
    }
}