<?php
namespace App\Repositories\Contracts;

interface MessagesRepositoryInterface{

    public function unreadMessagesForCurrentUser($limit = null);

    public function getOne($id);
}