<?php
namespace App\Repositories\Contracts;

use Illuminate\Http\Request;

interface UserRepositoryInterface{
    
    public function findByProperty($property, $value);

    public function saveBasedOnRequest(Request $request, $user = null);

    public function generateToken();

    public function setPasswordHash(string $password);

    public function activate($user);
}