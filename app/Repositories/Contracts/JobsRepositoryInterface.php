<?php
namespace App\Repositories\Contracts;

interface JobsRepositoryInterface{

    public function allActive($limit = null);

    public function createdByCurrentUser($limit = null);
}