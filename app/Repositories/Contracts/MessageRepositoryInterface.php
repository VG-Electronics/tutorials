<?php
namespace App\Repositories\Contracts;

interface MessageRepositoryInterface{

    public function saveAsRead($message);
}