<?php
namespace App\Repositories\Contracts;

interface JobRepositoryInterface{

    public function saveBasedOnRequest(\Request $request, $job = null);

    public function delete($job);
}