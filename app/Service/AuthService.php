<?php
namespace App\Service;

use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;

use App\Traits\Auth\LogInOut;

use Illuminate\Support\Facades\Auth;

use App\Mail\AccountActivation;

class AuthService{
    use LogInOut;

    const Employer = 1;

    public function __construct(UserRepositoryInterface $userRepository){
        $this->userRepository = $userRepository;
    }

    public function loginUserIfCredentialsCorrect(Request $request){
        
        $user = $this->userRepository->onlyActive(true)->findByProperty('email', $request->email);

        if( !$user || !password_verify($request->password, $user->password))
            return false;

        $this->logUserIn($user);

        return true;
    }

    public function registerUserAndSendActivationMail(Request $request){

        $user = $this->userRepository->saveBasedOnRequest($request);

        \Mail::to( $user->email )->send(new AccountActivation($user));

        return $user;
    }

    public function activateByTokenAndLogin(string $token){

        $user = $this->userRepository->findByProperty('token', $token);

        if( $user ){
            $this->userRepository->activate($user);

            // $this->logUserIn($user);
            if( !Auth::guard('employer')->check() )
                Auth::guard('employer')->login($user);

            return true;
        }

        return false;
    }
}