<?php

namespace App\Policies;

use App\Models\{Message, User};

use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;

    public function interaction(User $user, Message $message){
        return $message->receiver_id == $user->id;
    }
}
