<?php
namespace App\Models\Scopes;

use Carbon\Carbon;

trait JobScopes{

    public function scopeActive($query){
        return $query->where('active', true)->whereDate('active_to', '>', date('Y-m-d H:i:s'));
    }

    public function scopeCreatedThisWeek($query){
        return $query->whereDate('created_at', '>=', Carbon::now()->startOfWeek()->format('Y-m-d H:i:s'))
            ->whereDate('created_at', '<', date('Y-m-d H:i:s'));
    }
}