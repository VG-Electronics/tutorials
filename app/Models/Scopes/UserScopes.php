<?php
namespace App\Models\Scopes;

trait UserScopes{

    public function scopeActive($query){
        return $query->where('active', true);
    }

    public function scopeNotActive($query){
        return $query->where('active', false);
    }

    public function scopeEmail($query, $email){
        return $query->where('email', $email);
    }
}