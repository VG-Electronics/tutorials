<?php
namespace App\Models\Scopes;

trait MessageScopes{

    public function scopeRead($query){
        return $query->where('read', true);
    }

    public function scopeUnread($query){
        return $query->where('read', false);
    }
}