<?php
namespace App\Models\Relations;

use App\Models\{User};

trait MessageRelations{

    public function receiver(){
        return $this->belongsTo(User::class, 'receiver_id');
    }
}