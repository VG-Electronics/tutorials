<?php
namespace App\Models\Relations;

use App\Models\{Job, Message};

trait UserRelations{

    public function jobs(){
        return $this->hasMany(Job::class);
    }

    public function messages(){
        return $this->hasMany(Message::class, 'receiver_id');
    }
}