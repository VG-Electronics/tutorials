<?php
namespace App\Models\Relations;

use App\Models\{User};

trait JobRelations{

    public function user(){
        return $this->belongsTo(User::class);
    }
}