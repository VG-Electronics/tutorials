<?php

namespace App\Models;

use App\Models\Scopes\JobScopes;
use App\Models\Relations\JobRelations;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

use Carbon\Carbon;

class Job extends Model
{
    use SoftDeletes, JobScopes, JobRelations;

    protected $fillable = [
        'name', 'description', 'active_to'
    ];

    public function isActive(){
        return $this->active && $this->active_to > date('Y-m-d H:i:s');
    }

    public function activeTo(){
        return Carbon::parse($this->active_to)->format('Y-m-d H:i');
    }

    public function activeToForInput(){
        return Carbon::parse($this->active_to)->format('Y-m-d\TH:i');
    }
}
