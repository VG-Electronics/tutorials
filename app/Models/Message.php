<?php

namespace App\Models;

use App\Models\Scopes\MessageScopes;
use App\Models\Relations\MessageRelations;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use MessageScopes, MessageRelations;

    protected $fillable = [
        'title', 'content'
    ];
}
