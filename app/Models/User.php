<?php

namespace App\Models;

use App\Generators\TokenGenerator;
use App\Models\Scopes\UserScopes;
use App\Models\Relations\UserRelations;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, UserScopes, UserRelations;

    protected $fillable = [
        'first_name', 'last_name', 'email'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
