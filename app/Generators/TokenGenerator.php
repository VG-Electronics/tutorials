<?php
namespace App\Generators;

class TokenGenerator{

    public static function createToken($length = 20, $start = null){
        $chars = '0123456789abcdefghijklmnoprqstuvwxyz';
        $code = ($start ? $start : '').str_shuffle($chars);

        return substr($code, 0, $length);
    }
}