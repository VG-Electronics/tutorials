<?php
namespace App\Console;

use App\Console\Task;
use App\Console\Commands\{WeeklyReportForEmployer, Test00};

class JobsScheduler{

    public function __construct(){
        $this->commands = [
            Task::schedule( WeeklyReportForEmployer::class )->staticTime(7, 8, 0),
            Task::schedule( Test00::class )->dailyAt(0, 0),
        ];
    }

    public static function run(){
        $scheduler = new self();

        $scheduler->executeTasksForThisTime();
    }

    public function executeTasksForThisTime(){
        
        $tasksToExecute = [];
        
        foreach( $this->commands as $command ){
            if( $command->isTimeToExecute() )
                $tasksToExecute[] = $command;
        }

        foreach( $tasksToExecute as $task )
            $task->handle();
    }
}