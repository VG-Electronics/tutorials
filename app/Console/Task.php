<?php
namespace App\Console;

use App\Helpers\CarbonHelper;
use Illuminate\Console\Command;

class Task extends Command{

    public $intervalMinutes;
    public $staticTimes;

    public static function schedule($task){
        $task = new $task();

        return $task;
    }

    public function isTimeToExecute(){
        if( $this->intervalMinutes ){       
            $status = ( CarbonHelper::getMinutesSinceStartOfDay() % $this->intervalMinutes == 0 );
        }else{
            CarbonHelper::getDayOfWeekAndHourAndMinute($day, $hour, $minute);

            $status = false;
            foreach( $this->staticTimes as $time ){
                if( $day == $time[0] && $hour == $time[1] && $minute == $time[2] ){
                    $status = true;
                    break;
                }
            }
        }

        return $status ? true : false;
    }

    public function interval(int $minutes){
        $this->intervalMinutes = $minutes;
        $this->staticTimes = [];

        return $this;
    }

    public function staticTime(int $day, int $hour, int $minute){
        $this->validateStaticTime(null, $hour, $minute);

        if( $this->intervalMinutes )
            $this->intervalMinutes = false;
        
        $this->staticTimes[] = [$day, $hour, $minute];

        return $this;
    }

    public function dailyAt(int $hour, int $minute){
        $this->validateStaticTime(null, $hour, $minute);

        for($day = 7; $day != 0; $day--)
            $this->staticTimes[] = [$day, $hour, $minute];  

        return $this;
    }

    public function weekdaysAt(int $hour, int $minute){
        $this->validateStaticTime(null, $hour, $minute);

        for($day = 5; $day != 0; $day--)
            $this->staticTimes[] = [$day, $hour, $minute];

        return $this;
    }

    private function validateStaticTime($day = null, int $hour, int $minute){
        if( $day !== null && ($day <= 0 || $day > 7) )
            throw new \Exception('Day must be a number between 1 and 7.');

        if( $hour < 0 || $hour > 23 )
            throw new \Exception('Hour must be a number between 0 and 23.');

        if( $minute < 0 || $minute > 59 )
            throw new \Exception('Minute must be a number between 0 and 59.');
    }
}