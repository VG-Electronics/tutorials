<?php

namespace App\Console\Commands;

use App\Models\User;

use App\Console\Task;

class Test00 extends Task
{
    protected $signature = 'cron:test-00';
    protected $description = 'Command description';

    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users = User::Active()->get();

        foreach( $users as $user ){
            $user->messages()->create([
                'title' => 'CRON at midnight',
                'content' => 'Task run successfully. Current time - '.date('Y-m-d H:i:s')
            ]);
        }

        echo 'CRON at time 00:00 run successfully. Current time - '.date('Y-m-d H:i:s');
    }
}
