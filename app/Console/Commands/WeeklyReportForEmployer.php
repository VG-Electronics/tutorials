<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Console\Task;

use App\Mail\WeeklyReportForEmployer as ReportMail;

use Illuminate\Support\Facades\Mail;

class WeeklyReportForEmployer extends Task
{
    protected $signature = 'cron:weekly-reports';
    protected $description = 'Weekly reports for employers.';

    public function handle()
    {
        $users = User::Active()
            ->with(['jobs' => function($query){
                return $query->CreatedThisWeek()->get();
            }])
            ->get();

        foreach( $users as $user ){
            $jobsCount = count($user->jobs);

            Mail::to( $user->email )->send(new ReportMail($user));
            
            $user->messages()->create([
                'title' => 'Weekly report about your jobs.',
                'content' => 'In this week you have created '.$jobsCount.' '.($jobsCount == 1 ? 'job' : 'jobs').'.'
            ]);
        }

        echo 'Weekly report for employer: '.count($users).' mails and messages were sent |';
    }
}
