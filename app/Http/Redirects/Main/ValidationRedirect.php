<?php
namespace App\Http\Redirects\Main;

class ValidationRedirect extends AppRedirect{

    public function toPreviousPage(array $errors = [], $withInput = true){

        $redirect = redirect()->back();
        
        if( $errors )
            $redirect = $redirect->withErrors($errors);
        
        if( $withInput )
            $redirect = $redirect->withInput();

        return $redirect;
    }
}