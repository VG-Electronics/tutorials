<?php
namespace App\Http\Redirects\Main;

use App\Http\Routes\AppRoutes;

class AppRedirect extends ExceptionRedirect{
    use AppRoutes;

    public function toHomePage(){
        return redirect()->route( $this->appRoutes['home']['index'] );
    }
    
}