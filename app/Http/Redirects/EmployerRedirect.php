<?php
namespace App\Http\Redirects;

use App\Models\Job;

use App\Http\Redirects\Main\ValidationRedirect;

class EmployerRedirect extends ValidationRedirect{
    use \EmployerRoutes;

    public function toDashboard(){
        return redirect()->route( $this->employerRoutes['dashboard']['index'] );
    }

    public function toMyJobs(){
        return redirect()->route( $this->employerRoutes['jobs']['index'] );
    }

    public function toMyJob(Job $job){
        return redirect()->route( $this->employerRoutes['jobs']['show'], $job );
    }
}