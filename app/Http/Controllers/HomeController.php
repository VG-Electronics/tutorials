<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct(
        \AppViews $appViews
    ){
        $this->view = $appViews;
    }   

    public function index(){
        return $this->view->homePage();
    }
}
