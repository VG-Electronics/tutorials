<?php

namespace App\Http\Controllers;

use App\Service\AuthService;

use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function __construct(
        \AppViews $appView, \EmployerRedirect $redirect, 
        AuthService $authService
    ){

        $this->view = $appView;
        $this->redirect = $redirect;
        $this->auth = $authService;
    }

    public function index(){

        return $this->view->account();
    }

    public function login(Request $request){

        if( $this->auth->loginUserIfCredentialsCorrect($request) )
            return $this->redirect->toDashboard();

        return $this->redirect->toPreviousPage(['login' => 'Incorrect login data. Try to hint an another password.']);
    }

    public function register(Request $request){
        
        $user = $this->auth->registerUserAndSendActivationMail($request);

        $this->auth->logUserIn($user);

        return $this->redirect->toDashboard();
    }

    public function activate(string $token){

        if( $this->auth->activateByTokenAndLogin($token) )
            return $this->redirect->toDashboard();

        return $this->redirect->toPreviousPage([
            'login' => 'Incorrect activation link or account is already confirmed.'
        ]);
    }
}
