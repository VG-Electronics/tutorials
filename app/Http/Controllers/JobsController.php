<?php

namespace App\Http\Controllers;

use App\Models\Job;

class JobsController extends Controller
{
    public function __construct(
        \AppViews $appView,
        \JobsRepositoryInterface $jobs
    ){

        $this->view = $appView;

        $this->jobs = $jobs;
    }

    public function index(){
        $jobs = $this->jobs->allActive();

        return $this->view->jobs($jobs);
    }
}
