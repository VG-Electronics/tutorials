<?php

namespace App\Http\Controllers\employer;

use App\Service\AuthService;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function __construct(
        \EmployerRedirect $redirect,
        AuthService $authService
    ){
        $this->middleware('auth:employer');

        $this->auth = $authService;
        $this->redirect = $redirect;
    }

    public function logout(){

        $this->auth->logUserOut();

        return $this->redirect->toHomePage();
    }

    public function show(){
    
    }
    
    public function edit(){
    
    }
    
    public function update(Request $request){
    
    }
    
    public function delete(){
    
    }
}