<?php

namespace App\Http\Controllers\employer;

use App\Models\Message;

use App\Repositories\Contracts\{
    MessagesRepositoryInterface, MessageRepositoryInterface
};

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct(
        \EmployerViews $employerViews, \EmployerRedirect $employerRedirect,
        MessagesRepositoryInterface $messages, MessageRepositoryInterface $message
    ){
        $this->middleware('auth:employer');

        $this->view = $employerViews;
        $this->redirect = $employerRedirect;

        $this->message = $message;
        $this->messages = $messages;
    }

    public function index(){
        
        $messages = $this->messages->unreadMessagesForCurrentUser();
        
        return $this->view->dashboard($messages);
    }

    public function readMessage(Request $request){
        $message = $this->messages->getOne($request->messageID);
        
        if( !Auth::user()->can('interaction', $message) )
            return response()->json(['success' => false]);

        $this->message->saveAsRead($message);

        return response()->json(['success' => true]);
    }
}
