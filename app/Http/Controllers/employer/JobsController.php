<?php

namespace App\Http\Controllers\employer;

use App\Models\Job;

use App\Http\Controllers\Controller;

class JobsController extends Controller
{
    public function __construct(
        \EmployerViews $view, \EmployerRedirect $redirect,
        \JobRepositoryInterface $jobRepository, \JobsRepositoryInterface $jobsRepository
    ){
            
        $this->middleware('auth:employer');

        $this->view = $view;
        $this->redirect = $redirect;
        
        $this->jobRepository = $jobRepository;
        $this->jobsRepository = $jobsRepository;
    }

    public function index(){

        $jobs = $this->jobsRepository->createdByCurrentUser();
        
        return $this->view->myJobs($jobs);
    }

    public function show(Job $job){

        return $this->view->myJobShow($job);
    }
    
    public function create(){
    
        return $this->view->myJobCreate();
    }
    
    public function store(\Request $request){

        $job = $this->jobRepository->saveBasedOnRequest($request);
        
        return $this->redirect->toMyJob($job);
    }
    
    public function edit(Job $job){
        
        return $this->view->myJobEdit($job);
    }   
    
    public function update(Job $job, \Request $request){

        $job = $this->jobRepository->saveBasedOnRequest($request, $job);
        
        return $this->redirect->toMyJob($job);
    }
    
    public function delete(Job $job){

        $this->jobRepository->delete($job);
        
        return $this->redirect->toMyJobs();
    }
}
