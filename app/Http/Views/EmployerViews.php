<?php
namespace App\Http\Views;

use App\Models\Job;

class EmployerViews extends \AppViews{
    use \EmployerRoutes;

    public function dashboard($messages = []){
        return view( $this->employerRoutes['dashboard']['index'], compact('messages') );
    }

    public function myJobs($jobs = []){

        return view( $this->employerRoutes['jobs']['index'], compact('jobs'));
    }

    public function myJobShow(Job $job){
        return view( $this->employerRoutes['jobs']['show'], compact('job'));
    }
    
    public function myJobCreate(){
        return view( $this->employerRoutes['jobs']['create']);
    }

    public function myJobEdit(Job $job){
        return view( $this->employerRoutes['jobs']['edit'], compact('job'));
    }
}