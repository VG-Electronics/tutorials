<?php
namespace App\Http\Views\Main;

use App\Http\Routes\AppRoutes;

class AppViews{
    use AppRoutes;

    public function homePage(){
        return view( $this->appRoutes['home']['index'] );
    }

    public function jobs($jobs = null){       
        return view( $this->appRoutes['jobs']['index'], compact('jobs'));
    }
    
    public function account(){       
        return view( $this->appRoutes['account']['index'] );
    }
}