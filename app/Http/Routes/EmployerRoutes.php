<?php
namespace App\Http\Routes;

trait EmployerRoutes{

    protected $employerRoutes = [
        'dashboard' => [
            'index' => 'employer.dashboard.index'
        ],
        'jobs' => [
            'index' => 'employer.jobs.index',
            'show' => 'employer.jobs.show',
            'edit' => 'employer.jobs.edit',
            'create' => 'employer.jobs.create'
        ]
    ];
}