<?php
namespace App\Http\Routes;

trait AppRoutes{

    protected $appRoutes = [
        'home' => [
            'index' => 'home.index'
        ],
        'jobs' => [
            'index' => 'jobs.index'
        ],
        'account' => [
            'index' => 'account.index'
        ]
    ];
}