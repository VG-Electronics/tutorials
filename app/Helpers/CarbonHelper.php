<?php
namespace App\Helpers;

use Carbon\Carbon;

class CarbonHelper{

    public static function getDayOfWeekAndHourAndMinute(&$day, &$hour, &$minute){
        $currentTime = Carbon::now();

        $day = ($currentTime->dayOfWeek === Carbon::SUNDAY ? 7 : $currentTime->dayOfWeek);
        $hour = $currentTime->hour;
        $minute = $currentTime->minute;
    }

    public static function getMinutesSinceStartOfDay(){
        $zeroTime = Carbon::createFromTimeString('00:00:00');
        $minutes = Carbon::now()->diffInMinutes( $zeroTime );

        return $minutes;
    }
}