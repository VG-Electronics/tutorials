<?php
namespace App\Factories;

use App\Models\User;
use App\Generators\TokenGenerator;

use Illuminate\Http\Request;

class UserFactory extends Factory{

    private static $user;

    public static function createByRequest(Request $request){

        self::makeUserFromRequest($request);
        self::generateToken();
        self::setPasswordHash($request->password);
        self::$user->save();

        return self::$user;
    }

    public static function makeUserFromRequest(Request $request){
        self::$user = User::make( $request->all() );
    }

    public static function generateToken(){
        do{
            self::$user->token = $token = TokenGenerator::createToken();
        }while( User::where('token', $token)->exists() );
    }

    public static function setPasswordHash($password){
        self::$user->password = password_hash($password, PASSWORD_DEFAULT);
    }

    
}