<?php

namespace App\Mail;

use App\Models\{User, Job};

use Illuminate\Support\Collection;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WeeklyReportForEmployer extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->subject('Weekly report about your jobs')
            ->view('mail.employer.WeeklyReport')
            ->text('mail.employer.WeeklyReportTEXT')
            ->with('user', $this->user);
    }
}
