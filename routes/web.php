<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home.index');

Route::get('/jobs', 'JobsController@index')->name('jobs.index');

Route::middleware(['guest'])->group(function(){
    Route::get('/account', 'AccountController@index')->name('account.index');
    Route::post('/account/login', 'AccountController@login')->name('account.login');
    Route::post('/account/register', 'AccountController@register')->name('account.register');
});
Route::get('/account/activate/{token}', 'AccountController@activate')->name('account.activate');

Route::prefix('employer/')->group(function(){

    Route::get('logout', 'employer\AccountController@logout')->name('employer.account.logout');

    Route::get('dashboard', 'employer\DashboardController@index')->name('employer.dashboard.index');
    Route::post('dashboard/readMessage', 'employer\DashboardController@readMessage')->name('employer.dashboard.readMessage');

    Route::get('jobs', 'employer\JobsController@index')->name('employer.jobs.index');
    Route::get('jobs/show/{job}', 'employer\JobsController@show')->name('employer.jobs.show');
    Route::get('jobs/create', 'employer\JobsController@create')->name('employer.jobs.create');
    Route::post('jobs/store', 'employer\JobsController@store')->name('employer.jobs.store');
    Route::get('jobs/edit/{job}', 'employer\JobsController@edit')->name('employer.jobs.edit');
    Route::post('jobs/update/{job}', 'employer\JobsController@update')->name('employer.jobs.update');
    Route::delete('jobs/delete/{job}', 'employer\JobsController@delete')->name('employer.jobs.delete');

});
